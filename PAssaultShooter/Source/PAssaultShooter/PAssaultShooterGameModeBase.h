// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PAssaultShooterGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PASSAULTSHOOTER_API APAssaultShooterGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
