// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/PlayerInput.h"

// Sets default values
ABaseCharacter::ABaseCharacter(const FObjectInitializer& ObjInit) :
	Super(ObjInit.SetDefaultSubobjectClass<UAPCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoomCamera = CreateDefaultSubobject<USpringArmComponent>(TEXT("Boom Camera"));
	BoomCamera->SetupAttachment(RootComponent);
	BoomCamera->bUsePawnControlRotation = true;
	BoomCamera->SocketOffset = FVector(0,100.0,80.0f);

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Follow Camera"));
	FollowCamera->SetupAttachment(BoomCamera, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;
	
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("Health Component"));
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent);
	check(GetCharacterMovement());

	LandedDelegate.AddDynamic(this,&ABaseCharacter::OnGroundLanded);
	
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

static void InitializeInputs()
{
	bool bBindingAdded = false;

	if (!bBindingAdded)
	{
		bBindingAdded = true;


		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("Turn", EKeys::MouseX, 1.0f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("LookUp", EKeys::MouseY, 1.0f));

		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::W, 1.0f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveForward", EKeys::S, -1.0f));

		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::D, 1.0f));
		UPlayerInput::AddEngineDefinedAxisMapping(FInputAxisKeyMapping("MoveRight", EKeys::A, -1.0f));

		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Jump", EKeys::SpaceBar));
		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Run", EKeys::LeftShift));

		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Fire", EKeys::LeftMouseButton));


		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("ChangeCameraOffset", EKeys::L));

		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("NextWeapon", EKeys::Tab));
		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("NextWeapon", EKeys::MouseScrollUp));
		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("NextWeapon", EKeys::MouseScrollDown));

		UPlayerInput::AddEngineDefinedActionMapping(FInputActionKeyMapping("Reload", EKeys::R));

	}

}


// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{

	check(PlayerInputComponent != nullptr);

	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InitializeInputs();


	PlayerInputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &ABaseCharacter::Turn);
	PlayerInputComponent->BindAxis("LookUp", this, &ABaseCharacter::LookUp);

	PlayerInputComponent->BindAction("Run",IE_Pressed,this, &ABaseCharacter::OnStartRunning);
	PlayerInputComponent->BindAction("Run",IE_Released, this, &ABaseCharacter::OnStopRunning);

	PlayerInputComponent->BindAction("Jump",IE_Pressed,this,&ABaseCharacter::Jump);
}


#pragma region Movement Functions

void ABaseCharacter::MoveForward(float Value)
{
	bIsMovingForward = Value > 0.0f;

	if (Controller != nullptr && Value != 0.0f)
	{
		FRotator Rotation{ Controller->GetControlRotation() };
		FRotator Yaw{ 0.0f,Rotation.Yaw,0.0f };

		FVector Direction{ FRotationMatrix(Yaw).GetUnitAxis(EAxis::X) };

		AddMovementInput(Direction, Value);
	}
}

void ABaseCharacter::MoveRight(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		FRotator Rotation{ Controller->GetControlRotation() };
		FRotator Yaw{ 0.0f,Rotation.Yaw,0.0f };

		FVector Direction{ FRotationMatrix(Yaw).GetUnitAxis(EAxis::Y) };

		AddMovementInput(Direction, Value);
	}
}

void ABaseCharacter::Turn(float Rate)
{
	AddControllerYawInput(Rate);
}

void ABaseCharacter::LookUp(float Rate)
{
	AddControllerPitchInput(Rate);
}


#pragma endregion


#pragma region Animations Functions

bool ABaseCharacter::IsRunning() const
{
	return bWantsToRun && bIsMovingForward && !GetVelocity().IsZero();
}

void ABaseCharacter::ChangeSpringArmPosition()
{
}

void ABaseCharacter::OnStartRunning()
{
	bWantsToRun = true;
}

void ABaseCharacter::OnStopRunning()
{
	bWantsToRun = false;
}

#pragma endregion


#pragma region Health Functions

void ABaseCharacter::OnHealthChanged(float Health)
{
}


#pragma endregion

#pragma region Death Functions

void ABaseCharacter::OnDeath()
{
}

#pragma endregion

#pragma region Ground Land Damage Function

void ABaseCharacter::OnGroundLanded(const FHitResult& Hit)
{
	const auto FallVelocityZ = -GetCharacterMovement()->Velocity.Z;

	UE_LOG(LogTemp,Display,TEXT("On Landed: %f"),FallVelocityZ);

	if (FallVelocityZ < LandedDamageVelocity.X)
	{
		return;
	}

	const auto FinalDamage = FMath::GetMappedRangeValueClamped(LandedDamageVelocity, LandedDamage, FallVelocityZ);

	UE_LOG(LogTemp, Display, TEXT("Final Damage: %f"), FinalDamage);

	TakeDamage(FinalDamage, FDamageEvent{},nullptr,nullptr);

}



#pragma endregion