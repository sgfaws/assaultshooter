// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "../Components/HealthComponent.h"
#include "../Components/APCharacterMovementComponent.h"
#include "Animation/AnimMontage.h"
#include "BaseCharacter.generated.h"

UCLASS()
class PASSAULTSHOOTER_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Component)
		TObjectPtr<USpringArmComponent> BoomCamera = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Component)
		TObjectPtr<UCameraComponent> FollowCamera = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Component)
		float RateValue = 150.0f;

	//Health Component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HealthComponent)
		 TObjectPtr<UHealthComponent> HealthComponent = nullptr;


	//Animnation Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		uint8 bWantsToRun : 1;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		uint8 bIsMovingForward : 1;


	//Landed Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Land)
		FVector2D LandedDamageVelocity = FVector2D(900.0f,1200.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Land)
		FVector2D LandedDamage = FVector2D(10.0f, 100.0f);


	//Animation
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		TObjectPtr<UAnimMontage> DeathAnimMontage = nullptr;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

public:

	UFUNCTION()
		bool IsRunning() const;

	UFUNCTION()
		void ChangeSpringArmPosition();

	UFUNCTION()
		void OnStartRunning();

	UFUNCTION()
		void OnStopRunning();


private:

	UFUNCTION()
		void MoveForward(float Value);

	UFUNCTION()
		void MoveRight(float Value);

	UFUNCTION()
		void Turn(float Rate);

	UFUNCTION()
		void LookUp(float Rate);

	UFUNCTION()
		void OnDeath();

	UFUNCTION()
		void OnHealthChanged(float Health);

	UFUNCTION()
		void OnGroundLanded(const FHitResult& Hit);

};
