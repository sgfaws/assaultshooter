// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseWeapon.h"
#include "RifleWeapon.generated.h"

/**
 * 
 */
UCLASS()
class PASSAULTSHOOTER_API ARifleWeapon : public ABaseWeapon
{
	GENERATED_BODY()

public:

	ARifleWeapon();

	virtual void BeginPlay() override;

	virtual void Fire() override;

	virtual void StopFire() override;

	virtual void MakeShot() override;

	FTimerHandle ShotTimerHandle;

	UFUNCTION()
		void MakeDamage(const FHitResult& HitResult);

public:

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=RifleWeapon)
	float TimrBetweenShots = 0.1f;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RifleWeapon)
		float DamageAmount = 10.0f;

};
