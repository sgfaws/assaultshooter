// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "../../ASCoreTypes.h"
#include "BaseWeapon.generated.h"

UCLASS()
class PASSAULTSHOOTER_API ABaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		TObjectPtr<USkeletalMeshComponent> WeaponMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		FName MuzzleSocketName = "MuzzleSocket";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float TraceMaxDistance = 1500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		float Radius = 10.0f;


	//Ammo Data Variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
	FAmmoData DefaultAmmo{ 15,10,false };

	FAmmoData CurrentAmmo;




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:

	UFUNCTION()
		virtual void Fire();

	UFUNCTION()
		virtual void StopFire();

	UFUNCTION()
		virtual void MakeShot();

	UFUNCTION()
		APlayerController* GetPlayerController() const;


	UFUNCTION()
		bool GetPlayerViewPoint(FVector& ViewLocation,FRotator& ViewRotation) const;

	UFUNCTION()
		FVector GetMuzzleWorldLocation() const;

	UFUNCTION()
		virtual bool GetTraceData(FVector& TraceStart,FVector& TraceEnd) const;

	UFUNCTION()
		void MakeHit(FHitResult& HitResult,const FVector& TraceStart,const FVector& TraceEnd);


	//Ammo Functions Declaration
	UFUNCTION()
		void DecreaseAmmo();

	UFUNCTION()
		bool IsAmmoEmpty() const;

	UFUNCTION()
		bool IsClipEmpty() const;

	UFUNCTION()
		void ChangeClip();

	UFUNCTION()
		void LogAmmo();

	UFUNCTION()
		bool CanReload() const;


	UFUNCTION()
		FAmmoData GetAmmoData() const { return CurrentAmmo; }

};
