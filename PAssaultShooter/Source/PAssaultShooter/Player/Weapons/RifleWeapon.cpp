// Fill out your copyright notice in the Description page of Project Settings.


#include "RifleWeapon.h"
#include "DrawDebugHelpers.h"

ARifleWeapon::ARifleWeapon()
{

}

void ARifleWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void ARifleWeapon::Fire()
{
	GetWorldTimerManager().SetTimer(ShotTimerHandle,this,&ARifleWeapon::MakeShot,TimrBetweenShots,true);

	MakeShot();
}

void ARifleWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHandle);
}

void ARifleWeapon::MakeShot()
{
	if (!GetWorld() || IsAmmoEmpty())
	{
		StopFire();
		return;
	}

	FVector TraceStart, TraceEnd;

	if (!GetTraceData(TraceStart,TraceEnd))
	{
		StopFire();
		return;
	}

	FHitResult HitResult;

	MakeHit(HitResult,TraceStart,TraceEnd);


	if (HitResult.bBlockingHit)
	{
		MakeDamage(HitResult);
	}
	else {
		DrawDebugLine(GetWorld(),GetMuzzleWorldLocation(),TraceEnd,FColor::Green,false,3.0f,0.0f,3.0f);
	}


	DecreaseAmmo();
}

void ARifleWeapon::MakeDamage(const FHitResult& HitResult)
{
	const auto DamagedActor = HitResult.GetActor();

	if (!DamagedActor) return;

	DamagedActor->TakeDamage(DamageAmount,FDamageEvent(),GetPlayerController(),this);
}
