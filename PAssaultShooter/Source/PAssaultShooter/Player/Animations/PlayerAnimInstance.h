// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "../Characters/BaseCharacter.h"
#include "PlayerAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class PASSAULTSHOOTER_API UPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = Animation)
		void BlueprintUpdateProperties(float DeltaTime);
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category=Animation)
	ABaseCharacter* Player = nullptr;


	virtual void NativeInitializeAnimation() override;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		float Speed = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		float Direction = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		FVector Velocity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		uint8 bIsInAir : 1;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		uint8 bIsRunning : 1;

	//Variables for more movement management
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		float MovementDirectionAngle = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		FRotator AimRotation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		float AimRotationPitch = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animation)
		float AimRotationYaw = 0.0f;


};
