// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"

void UPlayerAnimInstance::NativeInitializeAnimation()
{
	if (Player == nullptr)
	{
		Player = Cast<ABaseCharacter>(TryGetPawnOwner());
	}
}


void UPlayerAnimInstance::BlueprintUpdateProperties(float DeltaTime)
{
	Player = Cast<ABaseCharacter>(TryGetPawnOwner());

	if (!Player) return;

	Velocity = Player->GetVelocity();
	Speed = Velocity.Size();

	/*FRotator Rotation = Player->GetControlRotation();

	FVector EndLine = */

	AimRotation = Player->GetBaseAimRotation();

	AimRotationPitch = AimRotation.Pitch;

	if (Velocity.IsZero())
	{
		MovementDirectionAngle = 0.0f;
	}


	float DotProduct = FVector::DotProduct(Player->GetActorForwardVector(),Velocity.GetSafeNormal());

	FVector CrossProduct = FVector::CrossProduct(Player->GetActorForwardVector(), Velocity.GetSafeNormal());

	float AcosInDegress = FMath::Acos(DotProduct) * 180.0f / 3.1416f;

	MovementDirectionAngle = FMath::Sign(CrossProduct.Z) * AcosInDegress;

	Direction = MovementDirectionAngle;

	bIsInAir = Player->GetCharacterMovement()->IsFalling();

	bIsRunning = Player->IsRunning();
}