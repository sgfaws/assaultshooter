// Fill out your copyright notice in the Description page of Project Settings.


#include "APCharacterMovementComponent.h"

float UAPCharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();

	const ABaseCharacter* Player = Cast<ABaseCharacter>(GetPawnOwner());

	return Player && Player->IsRunning() ? MaxSpeed * RunModifier : MaxSpeed;
}
