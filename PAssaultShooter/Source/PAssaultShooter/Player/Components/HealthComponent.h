// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../../ASCoreTypes.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PASSAULTSHOOTER_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, meta = (ClampMin = "0.0", ClampMax = "1000.0"))
		float MaxHealth = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health)
		uint8 bAutoHeal : 1;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, meta = (EditCondition = "bAutoHeal"))
		float HealUpdateTime = 1.0f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, meta = (EditCondition = "bAutoHeal"))
		float HealDelay = 3.0f;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Health, meta = (EditCondition = "bAutoHeal"))
		float HealModifier = 5.0f;


	float Health = 0.0f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
		float GetHealth() const { return Health; }


	UFUNCTION(BlueprintCallable,Category=Health)
		uint8 IsDead() const { return FMath::IsNearlyZero(Health); }

	UFUNCTION(BlueprintCallable, Category = Health)
		float GetHealthPercent() { return Health / MaxHealth; }

	UFUNCTION()
		void HealthUpdateBehaviour();

	UFUNCTION()
		void SetHealth(float NewHealth);



	UFUNCTION()
		void OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	//PickUp Add Health Functions
	UFUNCTION()
		bool TryToAddHealth(float HealthAmount);

	UFUNCTION()
		bool IsHealthFull() const;


	//Delegates
	FOnDeath OnDeath;
	FOnHealthChanged OnHealthChanged;

	FTimerHandle HealTimerHandle;
};
