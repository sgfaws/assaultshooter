// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "../Characters/BaseCharacter.h"
#include "APCharacterMovementComponent.generated.h"

/**
 * 
 */
UCLASS()
class PASSAULTSHOOTER_API UAPCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (ClampMin = "1.5", ClampMax = "10.0"))
		float RunModifier = 2.0f;


	virtual float GetMaxSpeed() const override;
	

	//class ABaseCharacter* Player = nullptr;
};
