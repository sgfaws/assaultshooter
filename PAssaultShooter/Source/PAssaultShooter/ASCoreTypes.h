// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "ASCoreTypes.generated.h"

//Base Weapon
class ABaseWeapon;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnClipEmptySignature, ABaseWeapon*);

USTRUCT(BlueprintType)
struct FAmmoData
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		int32 Bullets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon, meta = (EditCondition = "!Infinite"))
		int32 Clips;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Weapon)
		uint8 Infinite : 1;
};




//Health Component
DECLARE_MULTICAST_DELEGATE(FOnDeath)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHealthChanged,float)


class PASSAULTSHOOTER_API ASCoreTypes
{
public:
	ASCoreTypes();
	~ASCoreTypes();
};
