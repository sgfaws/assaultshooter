// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PASSAULTSHOOTER_PlayerAnimInstance_generated_h
#error "PlayerAnimInstance.generated.h already included, missing '#pragma once' in PlayerAnimInstance.h"
#endif
#define PASSAULTSHOOTER_PlayerAnimInstance_generated_h

#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_SPARSE_DATA
#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBlueprintUpdateProperties);


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBlueprintUpdateProperties);


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerAnimInstance(); \
	friend struct Z_Construct_UClass_UPlayerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPlayerAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(UPlayerAnimInstance)


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerAnimInstance(); \
	friend struct Z_Construct_UClass_UPlayerAnimInstance_Statics; \
public: \
	DECLARE_CLASS(UPlayerAnimInstance, UAnimInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(UPlayerAnimInstance)


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerAnimInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerAnimInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerAnimInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerAnimInstance(UPlayerAnimInstance&&); \
	NO_API UPlayerAnimInstance(const UPlayerAnimInstance&); \
public:


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerAnimInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerAnimInstance(UPlayerAnimInstance&&); \
	NO_API UPlayerAnimInstance(const UPlayerAnimInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerAnimInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerAnimInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerAnimInstance)


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_PRIVATE_PROPERTY_OFFSET
#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_13_PROLOG
#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_RPC_WRAPPERS \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_INCLASS \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_INCLASS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PASSAULTSHOOTER_API UClass* StaticClass<class UPlayerAnimInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_Player_Animations_PlayerAnimInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
