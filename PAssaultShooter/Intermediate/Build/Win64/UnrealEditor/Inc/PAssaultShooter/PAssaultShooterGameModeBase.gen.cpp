// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PAssaultShooter/PAssaultShooterGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePAssaultShooterGameModeBase() {}
// Cross Module References
	PASSAULTSHOOTER_API UClass* Z_Construct_UClass_APAssaultShooterGameModeBase_NoRegister();
	PASSAULTSHOOTER_API UClass* Z_Construct_UClass_APAssaultShooterGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PAssaultShooter();
// End Cross Module References
	void APAssaultShooterGameModeBase::StaticRegisterNativesAPAssaultShooterGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_APAssaultShooterGameModeBase_NoRegister()
	{
		return APAssaultShooterGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APAssaultShooterGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PAssaultShooter,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering LOD WorldPartition DataLayers Utilities|Transformation" },
		{ "IncludePath", "PAssaultShooterGameModeBase.h" },
		{ "ModuleRelativePath", "PAssaultShooterGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APAssaultShooterGameModeBase>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::ClassParams = {
		&APAssaultShooterGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APAssaultShooterGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UECodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APAssaultShooterGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APAssaultShooterGameModeBase, 743979550);
	template<> PASSAULTSHOOTER_API UClass* StaticClass<APAssaultShooterGameModeBase>()
	{
		return APAssaultShooterGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APAssaultShooterGameModeBase(Z_Construct_UClass_APAssaultShooterGameModeBase, &APAssaultShooterGameModeBase::StaticClass, TEXT("/Script/PAssaultShooter"), TEXT("APAssaultShooterGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APAssaultShooterGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
