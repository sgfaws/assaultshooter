// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PASSAULTSHOOTER_ASCoreTypes_generated_h
#error "ASCoreTypes.generated.h already included, missing '#pragma once' in ASCoreTypes.h"
#endif
#define PASSAULTSHOOTER_ASCoreTypes_generated_h

#define PAssaultShooter_Source_PAssaultShooter_ASCoreTypes_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAmmoData_Statics; \
	PASSAULTSHOOTER_API static class UScriptStruct* StaticStruct();


template<> PASSAULTSHOOTER_API UScriptStruct* StaticStruct<struct FAmmoData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_ASCoreTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
