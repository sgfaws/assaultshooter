// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
#ifdef PASSAULTSHOOTER_BaseCharacter_generated_h
#error "BaseCharacter.generated.h already included, missing '#pragma once' in BaseCharacter.h"
#endif
#define PASSAULTSHOOTER_BaseCharacter_generated_h

#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_SPARSE_DATA
#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnGroundLanded); \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execOnDeath); \
	DECLARE_FUNCTION(execLookUp); \
	DECLARE_FUNCTION(execTurn); \
	DECLARE_FUNCTION(execMoveRight); \
	DECLARE_FUNCTION(execMoveForward); \
	DECLARE_FUNCTION(execOnStopRunning); \
	DECLARE_FUNCTION(execOnStartRunning); \
	DECLARE_FUNCTION(execChangeSpringArmPosition); \
	DECLARE_FUNCTION(execIsRunning);


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnGroundLanded); \
	DECLARE_FUNCTION(execOnHealthChanged); \
	DECLARE_FUNCTION(execOnDeath); \
	DECLARE_FUNCTION(execLookUp); \
	DECLARE_FUNCTION(execTurn); \
	DECLARE_FUNCTION(execMoveRight); \
	DECLARE_FUNCTION(execMoveForward); \
	DECLARE_FUNCTION(execOnStopRunning); \
	DECLARE_FUNCTION(execOnStartRunning); \
	DECLARE_FUNCTION(execChangeSpringArmPosition); \
	DECLARE_FUNCTION(execIsRunning);


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseCharacter(); \
	friend struct Z_Construct_UClass_ABaseCharacter_Statics; \
public: \
	DECLARE_CLASS(ABaseCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseCharacter)


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesABaseCharacter(); \
	friend struct Z_Construct_UClass_ABaseCharacter_Statics; \
public: \
	DECLARE_CLASS(ABaseCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseCharacter)


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseCharacter(ABaseCharacter&&); \
	NO_API ABaseCharacter(const ABaseCharacter&); \
public:


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseCharacter(ABaseCharacter&&); \
	NO_API ABaseCharacter(const ABaseCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseCharacter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseCharacter)


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_PRIVATE_PROPERTY_OFFSET
#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_14_PROLOG
#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_RPC_WRAPPERS \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_INCLASS \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_INCLASS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PASSAULTSHOOTER_API UClass* StaticClass<class ABaseCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_Player_Characters_BaseCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
