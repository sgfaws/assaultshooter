// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FAmmoData;
struct FHitResult;
struct FVector;
struct FRotator;
class APlayerController;
#ifdef PASSAULTSHOOTER_BaseWeapon_generated_h
#error "BaseWeapon.generated.h already included, missing '#pragma once' in BaseWeapon.h"
#endif
#define PASSAULTSHOOTER_BaseWeapon_generated_h

#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_SPARSE_DATA
#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAmmoData); \
	DECLARE_FUNCTION(execCanReload); \
	DECLARE_FUNCTION(execLogAmmo); \
	DECLARE_FUNCTION(execChangeClip); \
	DECLARE_FUNCTION(execIsClipEmpty); \
	DECLARE_FUNCTION(execIsAmmoEmpty); \
	DECLARE_FUNCTION(execDecreaseAmmo); \
	DECLARE_FUNCTION(execMakeHit); \
	DECLARE_FUNCTION(execGetTraceData); \
	DECLARE_FUNCTION(execGetMuzzleWorldLocation); \
	DECLARE_FUNCTION(execGetPlayerViewPoint); \
	DECLARE_FUNCTION(execGetPlayerController); \
	DECLARE_FUNCTION(execMakeShot); \
	DECLARE_FUNCTION(execStopFire); \
	DECLARE_FUNCTION(execFire);


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAmmoData); \
	DECLARE_FUNCTION(execCanReload); \
	DECLARE_FUNCTION(execLogAmmo); \
	DECLARE_FUNCTION(execChangeClip); \
	DECLARE_FUNCTION(execIsClipEmpty); \
	DECLARE_FUNCTION(execIsAmmoEmpty); \
	DECLARE_FUNCTION(execDecreaseAmmo); \
	DECLARE_FUNCTION(execMakeHit); \
	DECLARE_FUNCTION(execGetTraceData); \
	DECLARE_FUNCTION(execGetMuzzleWorldLocation); \
	DECLARE_FUNCTION(execGetPlayerViewPoint); \
	DECLARE_FUNCTION(execGetPlayerController); \
	DECLARE_FUNCTION(execMakeShot); \
	DECLARE_FUNCTION(execStopFire); \
	DECLARE_FUNCTION(execFire);


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABaseWeapon(); \
	friend struct Z_Construct_UClass_ABaseWeapon_Statics; \
public: \
	DECLARE_CLASS(ABaseWeapon, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseWeapon)


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABaseWeapon(); \
	friend struct Z_Construct_UClass_ABaseWeapon_Statics; \
public: \
	DECLARE_CLASS(ABaseWeapon, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(ABaseWeapon)


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABaseWeapon(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABaseWeapon) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseWeapon); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseWeapon); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseWeapon(ABaseWeapon&&); \
	NO_API ABaseWeapon(const ABaseWeapon&); \
public:


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABaseWeapon(ABaseWeapon&&); \
	NO_API ABaseWeapon(const ABaseWeapon&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABaseWeapon); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABaseWeapon); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABaseWeapon)


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_PRIVATE_PROPERTY_OFFSET
#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_11_PROLOG
#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_RPC_WRAPPERS \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_INCLASS \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_INCLASS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PASSAULTSHOOTER_API UClass* StaticClass<class ABaseWeapon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_Player_Weapons_BaseWeapon_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
