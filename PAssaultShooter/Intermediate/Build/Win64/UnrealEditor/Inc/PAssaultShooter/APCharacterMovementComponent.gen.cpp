// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PAssaultShooter/Player/Components/APCharacterMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAPCharacterMovementComponent() {}
// Cross Module References
	PASSAULTSHOOTER_API UClass* Z_Construct_UClass_UAPCharacterMovementComponent_NoRegister();
	PASSAULTSHOOTER_API UClass* Z_Construct_UClass_UAPCharacterMovementComponent();
	ENGINE_API UClass* Z_Construct_UClass_UCharacterMovementComponent();
	UPackage* Z_Construct_UPackage__Script_PAssaultShooter();
// End Cross Module References
	void UAPCharacterMovementComponent::StaticRegisterNativesUAPCharacterMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_UAPCharacterMovementComponent_NoRegister()
	{
		return UAPCharacterMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UAPCharacterMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_RunModifier_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_RunModifier;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAPCharacterMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCharacterMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PAssaultShooter,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPCharacterMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Player/Components/APCharacterMovementComponent.h" },
		{ "ModuleRelativePath", "Player/Components/APCharacterMovementComponent.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAPCharacterMovementComponent_Statics::NewProp_RunModifier_MetaData[] = {
		{ "Category", "Movement" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "1.5" },
		{ "ModuleRelativePath", "Player/Components/APCharacterMovementComponent.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAPCharacterMovementComponent_Statics::NewProp_RunModifier = { "RunModifier", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAPCharacterMovementComponent, RunModifier), METADATA_PARAMS(Z_Construct_UClass_UAPCharacterMovementComponent_Statics::NewProp_RunModifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAPCharacterMovementComponent_Statics::NewProp_RunModifier_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAPCharacterMovementComponent_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAPCharacterMovementComponent_Statics::NewProp_RunModifier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAPCharacterMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAPCharacterMovementComponent>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UAPCharacterMovementComponent_Statics::ClassParams = {
		&UAPCharacterMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAPCharacterMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAPCharacterMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UAPCharacterMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAPCharacterMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAPCharacterMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UECodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAPCharacterMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAPCharacterMovementComponent, 3755707941);
	template<> PASSAULTSHOOTER_API UClass* StaticClass<UAPCharacterMovementComponent>()
	{
		return UAPCharacterMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAPCharacterMovementComponent(Z_Construct_UClass_UAPCharacterMovementComponent, &UAPCharacterMovementComponent::StaticClass, TEXT("/Script/PAssaultShooter"), TEXT("UAPCharacterMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAPCharacterMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
