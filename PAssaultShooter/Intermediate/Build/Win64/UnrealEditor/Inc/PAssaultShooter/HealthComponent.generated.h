// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef PASSAULTSHOOTER_HealthComponent_generated_h
#error "HealthComponent.generated.h already included, missing '#pragma once' in HealthComponent.h"
#endif
#define PASSAULTSHOOTER_HealthComponent_generated_h

#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_SPARSE_DATA
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsHealthFull); \
	DECLARE_FUNCTION(execTryToAddHealth); \
	DECLARE_FUNCTION(execOnTakeAnyDamageHandle); \
	DECLARE_FUNCTION(execSetHealth); \
	DECLARE_FUNCTION(execHealthUpdateBehaviour); \
	DECLARE_FUNCTION(execGetHealthPercent); \
	DECLARE_FUNCTION(execIsDead); \
	DECLARE_FUNCTION(execGetHealth);


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsHealthFull); \
	DECLARE_FUNCTION(execTryToAddHealth); \
	DECLARE_FUNCTION(execOnTakeAnyDamageHandle); \
	DECLARE_FUNCTION(execSetHealth); \
	DECLARE_FUNCTION(execHealthUpdateBehaviour); \
	DECLARE_FUNCTION(execGetHealthPercent); \
	DECLARE_FUNCTION(execIsDead); \
	DECLARE_FUNCTION(execGetHealth);


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealthComponent(); \
	friend struct Z_Construct_UClass_UHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(UHealthComponent)


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUHealthComponent(); \
	friend struct Z_Construct_UClass_UHealthComponent_Statics; \
public: \
	DECLARE_CLASS(UHealthComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(UHealthComponent)


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealthComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealthComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthComponent(UHealthComponent&&); \
	NO_API UHealthComponent(const UHealthComponent&); \
public:


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealthComponent(UHealthComponent&&); \
	NO_API UHealthComponent(const UHealthComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealthComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealthComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHealthComponent)


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_PRIVATE_PROPERTY_OFFSET
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_11_PROLOG
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_RPC_WRAPPERS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_INCLASS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_INCLASS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PASSAULTSHOOTER_API UClass* StaticClass<class UHealthComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_Player_Components_HealthComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
