// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PASSAULTSHOOTER_PAssaultShooterGameModeBase_generated_h
#error "PAssaultShooterGameModeBase.generated.h already included, missing '#pragma once' in PAssaultShooterGameModeBase.h"
#endif
#define PASSAULTSHOOTER_PAssaultShooterGameModeBase_generated_h

#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_SPARSE_DATA
#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_RPC_WRAPPERS
#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPAssaultShooterGameModeBase(); \
	friend struct Z_Construct_UClass_APAssaultShooterGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APAssaultShooterGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(APAssaultShooterGameModeBase)


#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPAssaultShooterGameModeBase(); \
	friend struct Z_Construct_UClass_APAssaultShooterGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APAssaultShooterGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(APAssaultShooterGameModeBase)


#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APAssaultShooterGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APAssaultShooterGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APAssaultShooterGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APAssaultShooterGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APAssaultShooterGameModeBase(APAssaultShooterGameModeBase&&); \
	NO_API APAssaultShooterGameModeBase(const APAssaultShooterGameModeBase&); \
public:


#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APAssaultShooterGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APAssaultShooterGameModeBase(APAssaultShooterGameModeBase&&); \
	NO_API APAssaultShooterGameModeBase(const APAssaultShooterGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APAssaultShooterGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APAssaultShooterGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APAssaultShooterGameModeBase)


#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_12_PROLOG
#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_RPC_WRAPPERS \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_INCLASS \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PASSAULTSHOOTER_API UClass* StaticClass<class APAssaultShooterGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_PAssaultShooterGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
