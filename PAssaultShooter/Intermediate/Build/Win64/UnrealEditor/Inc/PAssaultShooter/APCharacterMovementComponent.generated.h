// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PASSAULTSHOOTER_APCharacterMovementComponent_generated_h
#error "APCharacterMovementComponent.generated.h already included, missing '#pragma once' in APCharacterMovementComponent.h"
#endif
#define PASSAULTSHOOTER_APCharacterMovementComponent_generated_h

#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_SPARSE_DATA
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_RPC_WRAPPERS
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAPCharacterMovementComponent(); \
	friend struct Z_Construct_UClass_UAPCharacterMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UAPCharacterMovementComponent, UCharacterMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(UAPCharacterMovementComponent)


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUAPCharacterMovementComponent(); \
	friend struct Z_Construct_UClass_UAPCharacterMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UAPCharacterMovementComponent, UCharacterMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PAssaultShooter"), NO_API) \
	DECLARE_SERIALIZER(UAPCharacterMovementComponent)


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAPCharacterMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAPCharacterMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAPCharacterMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAPCharacterMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAPCharacterMovementComponent(UAPCharacterMovementComponent&&); \
	NO_API UAPCharacterMovementComponent(const UAPCharacterMovementComponent&); \
public:


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAPCharacterMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAPCharacterMovementComponent(UAPCharacterMovementComponent&&); \
	NO_API UAPCharacterMovementComponent(const UAPCharacterMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAPCharacterMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAPCharacterMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAPCharacterMovementComponent)


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_PRIVATE_PROPERTY_OFFSET
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_13_PROLOG
#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_RPC_WRAPPERS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_INCLASS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_PRIVATE_PROPERTY_OFFSET \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_SPARSE_DATA \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_INCLASS_NO_PURE_DECLS \
	PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PASSAULTSHOOTER_API UClass* StaticClass<class UAPCharacterMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PAssaultShooter_Source_PAssaultShooter_Player_Components_APCharacterMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
