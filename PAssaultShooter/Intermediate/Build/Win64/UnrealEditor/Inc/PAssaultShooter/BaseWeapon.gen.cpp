// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PAssaultShooter/Player/Weapons/BaseWeapon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBaseWeapon() {}
// Cross Module References
	PASSAULTSHOOTER_API UClass* Z_Construct_UClass_ABaseWeapon_NoRegister();
	PASSAULTSHOOTER_API UClass* Z_Construct_UClass_ABaseWeapon();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_PAssaultShooter();
	PASSAULTSHOOTER_API UScriptStruct* Z_Construct_UScriptStruct_FAmmoData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FHitResult();
	ENGINE_API UClass* Z_Construct_UClass_USkeletalMeshComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABaseWeapon::execGetAmmoData)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FAmmoData*)Z_Param__Result=P_THIS->GetAmmoData();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execCanReload)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->CanReload();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execLogAmmo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->LogAmmo();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execChangeClip)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ChangeClip();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execIsClipEmpty)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsClipEmpty();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execIsAmmoEmpty)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsAmmoEmpty();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execDecreaseAmmo)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DecreaseAmmo();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execMakeHit)
	{
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_HitResult);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_TraceStart);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_TraceEnd);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MakeHit(Z_Param_Out_HitResult,Z_Param_Out_TraceStart,Z_Param_Out_TraceEnd);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execGetTraceData)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_TraceStart);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_TraceEnd);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetTraceData(Z_Param_Out_TraceStart,Z_Param_Out_TraceEnd);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execGetMuzzleWorldLocation)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector*)Z_Param__Result=P_THIS->GetMuzzleWorldLocation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execGetPlayerViewPoint)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_ViewLocation);
		P_GET_STRUCT_REF(FRotator,Z_Param_Out_ViewRotation);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetPlayerViewPoint(Z_Param_Out_ViewLocation,Z_Param_Out_ViewRotation);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execGetPlayerController)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(APlayerController**)Z_Param__Result=P_THIS->GetPlayerController();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execMakeShot)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MakeShot();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execStopFire)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StopFire();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABaseWeapon::execFire)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Fire();
		P_NATIVE_END;
	}
	void ABaseWeapon::StaticRegisterNativesABaseWeapon()
	{
		UClass* Class = ABaseWeapon::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CanReload", &ABaseWeapon::execCanReload },
			{ "ChangeClip", &ABaseWeapon::execChangeClip },
			{ "DecreaseAmmo", &ABaseWeapon::execDecreaseAmmo },
			{ "Fire", &ABaseWeapon::execFire },
			{ "GetAmmoData", &ABaseWeapon::execGetAmmoData },
			{ "GetMuzzleWorldLocation", &ABaseWeapon::execGetMuzzleWorldLocation },
			{ "GetPlayerController", &ABaseWeapon::execGetPlayerController },
			{ "GetPlayerViewPoint", &ABaseWeapon::execGetPlayerViewPoint },
			{ "GetTraceData", &ABaseWeapon::execGetTraceData },
			{ "IsAmmoEmpty", &ABaseWeapon::execIsAmmoEmpty },
			{ "IsClipEmpty", &ABaseWeapon::execIsClipEmpty },
			{ "LogAmmo", &ABaseWeapon::execLogAmmo },
			{ "MakeHit", &ABaseWeapon::execMakeHit },
			{ "MakeShot", &ABaseWeapon::execMakeShot },
			{ "StopFire", &ABaseWeapon::execStopFire },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABaseWeapon_CanReload_Statics
	{
		struct BaseWeapon_eventCanReload_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BaseWeapon_eventCanReload_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BaseWeapon_eventCanReload_Parms), &Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "CanReload", nullptr, nullptr, sizeof(BaseWeapon_eventCanReload_Parms), Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_CanReload()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_CanReload_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_ChangeClip_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_ChangeClip_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_ChangeClip_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "ChangeClip", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_ChangeClip_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_ChangeClip_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_ChangeClip()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_ChangeClip_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo_Statics::Function_MetaDataParams[] = {
		{ "Comment", "//Ammo Functions Declaration\n" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
		{ "ToolTip", "Ammo Functions Declaration" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "DecreaseAmmo", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_Fire_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_Fire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_Fire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "Fire", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_Fire_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_Fire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_Fire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_Fire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics
	{
		struct BaseWeapon_eventGetAmmoData_Parms
		{
			FAmmoData ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetAmmoData_Parms, ReturnValue), Z_Construct_UScriptStruct_FAmmoData, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "GetAmmoData", nullptr, nullptr, sizeof(BaseWeapon_eventGetAmmoData_Parms), Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_GetAmmoData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_GetAmmoData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics
	{
		struct BaseWeapon_eventGetMuzzleWorldLocation_Parms
		{
			FVector ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetMuzzleWorldLocation_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "GetMuzzleWorldLocation", nullptr, nullptr, sizeof(BaseWeapon_eventGetMuzzleWorldLocation_Parms), Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics
	{
		struct BaseWeapon_eventGetPlayerController_Parms
		{
			APlayerController* ReturnValue;
		};
		static const UECodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetPlayerController_Parms, ReturnValue), Z_Construct_UClass_APlayerController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "GetPlayerController", nullptr, nullptr, sizeof(BaseWeapon_eventGetPlayerController_Parms), Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_GetPlayerController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_GetPlayerController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics
	{
		struct BaseWeapon_eventGetPlayerViewPoint_Parms
		{
			FVector ViewLocation;
			FRotator ViewRotation;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_ViewLocation;
		static const UECodeGen_Private::FStructPropertyParams NewProp_ViewRotation;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ViewLocation = { "ViewLocation", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetPlayerViewPoint_Parms, ViewLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ViewRotation = { "ViewRotation", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetPlayerViewPoint_Parms, ViewRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BaseWeapon_eventGetPlayerViewPoint_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BaseWeapon_eventGetPlayerViewPoint_Parms), &Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ViewLocation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ViewRotation,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "GetPlayerViewPoint", nullptr, nullptr, sizeof(BaseWeapon_eventGetPlayerViewPoint_Parms), Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics
	{
		struct BaseWeapon_eventGetTraceData_Parms
		{
			FVector TraceStart;
			FVector TraceEnd;
			bool ReturnValue;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_TraceStart;
		static const UECodeGen_Private::FStructPropertyParams NewProp_TraceEnd;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_TraceStart = { "TraceStart", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetTraceData_Parms, TraceStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_TraceEnd = { "TraceEnd", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventGetTraceData_Parms, TraceEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BaseWeapon_eventGetTraceData_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BaseWeapon_eventGetTraceData_Parms), &Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_TraceStart,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_TraceEnd,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "GetTraceData", nullptr, nullptr, sizeof(BaseWeapon_eventGetTraceData_Parms), Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_GetTraceData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_GetTraceData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics
	{
		struct BaseWeapon_eventIsAmmoEmpty_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BaseWeapon_eventIsAmmoEmpty_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BaseWeapon_eventIsAmmoEmpty_Parms), &Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "IsAmmoEmpty", nullptr, nullptr, sizeof(BaseWeapon_eventIsAmmoEmpty_Parms), Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics
	{
		struct BaseWeapon_eventIsClipEmpty_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BaseWeapon_eventIsClipEmpty_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BaseWeapon_eventIsClipEmpty_Parms), &Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "IsClipEmpty", nullptr, nullptr, sizeof(BaseWeapon_eventIsClipEmpty_Parms), Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x40020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_IsClipEmpty()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_IsClipEmpty_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_LogAmmo_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_LogAmmo_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_LogAmmo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "LogAmmo", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_LogAmmo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_LogAmmo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_LogAmmo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_LogAmmo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics
	{
		struct BaseWeapon_eventMakeHit_Parms
		{
			FHitResult HitResult;
			FVector TraceStart;
			FVector TraceEnd;
		};
		static const UECodeGen_Private::FStructPropertyParams NewProp_HitResult;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TraceStart_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_TraceStart;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TraceEnd_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_TraceEnd;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_HitResult = { "HitResult", nullptr, (EPropertyFlags)0x0010008000000180, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventMakeHit_Parms, HitResult), Z_Construct_UScriptStruct_FHitResult, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceStart_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceStart = { "TraceStart", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventMakeHit_Parms, TraceStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceStart_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceEnd_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceEnd = { "TraceEnd", nullptr, (EPropertyFlags)0x0010000008000182, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BaseWeapon_eventMakeHit_Parms, TraceEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceEnd_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_HitResult,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceStart,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::NewProp_TraceEnd,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "MakeHit", nullptr, nullptr, sizeof(BaseWeapon_eventMakeHit_Parms), Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_MakeHit()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_MakeHit_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_MakeShot_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_MakeShot_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_MakeShot_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "MakeShot", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_MakeShot_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_MakeShot_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_MakeShot()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_MakeShot_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABaseWeapon_StopFire_Statics
	{
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABaseWeapon_StopFire_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_ABaseWeapon_StopFire_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABaseWeapon, nullptr, "StopFire", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABaseWeapon_StopFire_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABaseWeapon_StopFire_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABaseWeapon_StopFire()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABaseWeapon_StopFire_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABaseWeapon_NoRegister()
	{
		return ABaseWeapon::StaticClass();
	}
	struct Z_Construct_UClass_ABaseWeapon_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_WeaponMesh_MetaData[];
#endif
		static const UECodeGen_Private::FObjectPtrPropertyParams NewProp_WeaponMesh;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_MuzzleSocketName_MetaData[];
#endif
		static const UECodeGen_Private::FNamePropertyParams NewProp_MuzzleSocketName;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_TraceMaxDistance_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_TraceMaxDistance;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_DefaultAmmo_MetaData[];
#endif
		static const UECodeGen_Private::FStructPropertyParams NewProp_DefaultAmmo;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABaseWeapon_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_PAssaultShooter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABaseWeapon_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABaseWeapon_CanReload, "CanReload" }, // 1390663369
		{ &Z_Construct_UFunction_ABaseWeapon_ChangeClip, "ChangeClip" }, // 321829117
		{ &Z_Construct_UFunction_ABaseWeapon_DecreaseAmmo, "DecreaseAmmo" }, // 1110128480
		{ &Z_Construct_UFunction_ABaseWeapon_Fire, "Fire" }, // 2635582353
		{ &Z_Construct_UFunction_ABaseWeapon_GetAmmoData, "GetAmmoData" }, // 1841410737
		{ &Z_Construct_UFunction_ABaseWeapon_GetMuzzleWorldLocation, "GetMuzzleWorldLocation" }, // 4144335089
		{ &Z_Construct_UFunction_ABaseWeapon_GetPlayerController, "GetPlayerController" }, // 706141831
		{ &Z_Construct_UFunction_ABaseWeapon_GetPlayerViewPoint, "GetPlayerViewPoint" }, // 3046229072
		{ &Z_Construct_UFunction_ABaseWeapon_GetTraceData, "GetTraceData" }, // 3281718966
		{ &Z_Construct_UFunction_ABaseWeapon_IsAmmoEmpty, "IsAmmoEmpty" }, // 2648533344
		{ &Z_Construct_UFunction_ABaseWeapon_IsClipEmpty, "IsClipEmpty" }, // 3982789225
		{ &Z_Construct_UFunction_ABaseWeapon_LogAmmo, "LogAmmo" }, // 3993529365
		{ &Z_Construct_UFunction_ABaseWeapon_MakeHit, "MakeHit" }, // 923551572
		{ &Z_Construct_UFunction_ABaseWeapon_MakeShot, "MakeShot" }, // 1904807188
		{ &Z_Construct_UFunction_ABaseWeapon_StopFire, "StopFire" }, // 930475088
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseWeapon_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Player/Weapons/BaseWeapon.h" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseWeapon_Statics::NewProp_WeaponMesh_MetaData[] = {
		{ "Category", "Weapon" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FObjectPtrPropertyParams Z_Construct_UClass_ABaseWeapon_Statics::NewProp_WeaponMesh = { "WeaponMesh", nullptr, (EPropertyFlags)0x001400000008000d, UECodeGen_Private::EPropertyGenFlags::Object | UECodeGen_Private::EPropertyGenFlags::ObjectPtr, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseWeapon, WeaponMesh), Z_Construct_UClass_USkeletalMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_WeaponMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_WeaponMesh_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseWeapon_Statics::NewProp_MuzzleSocketName_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FNamePropertyParams Z_Construct_UClass_ABaseWeapon_Statics::NewProp_MuzzleSocketName = { "MuzzleSocketName", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseWeapon, MuzzleSocketName), METADATA_PARAMS(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_MuzzleSocketName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_MuzzleSocketName_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseWeapon_Statics::NewProp_TraceMaxDistance_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABaseWeapon_Statics::NewProp_TraceMaxDistance = { "TraceMaxDistance", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseWeapon, TraceMaxDistance), METADATA_PARAMS(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_TraceMaxDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_TraceMaxDistance_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseWeapon_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "Weapon" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABaseWeapon_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseWeapon, Radius), METADATA_PARAMS(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABaseWeapon_Statics::NewProp_DefaultAmmo_MetaData[] = {
		{ "Category", "Weapon" },
		{ "Comment", "//Ammo Data Variables\n" },
		{ "ModuleRelativePath", "Player/Weapons/BaseWeapon.h" },
		{ "ToolTip", "Ammo Data Variables" },
	};
#endif
	const UECodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABaseWeapon_Statics::NewProp_DefaultAmmo = { "DefaultAmmo", nullptr, (EPropertyFlags)0x0010000000000005, UECodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABaseWeapon, DefaultAmmo), Z_Construct_UScriptStruct_FAmmoData, METADATA_PARAMS(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_DefaultAmmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::NewProp_DefaultAmmo_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABaseWeapon_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseWeapon_Statics::NewProp_WeaponMesh,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseWeapon_Statics::NewProp_MuzzleSocketName,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseWeapon_Statics::NewProp_TraceMaxDistance,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseWeapon_Statics::NewProp_Radius,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABaseWeapon_Statics::NewProp_DefaultAmmo,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABaseWeapon_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABaseWeapon>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ABaseWeapon_Statics::ClassParams = {
		&ABaseWeapon::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABaseWeapon_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABaseWeapon_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABaseWeapon_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABaseWeapon()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UECodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABaseWeapon_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABaseWeapon, 978646494);
	template<> PASSAULTSHOOTER_API UClass* StaticClass<ABaseWeapon>()
	{
		return ABaseWeapon::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABaseWeapon(Z_Construct_UClass_ABaseWeapon, &ABaseWeapon::StaticClass, TEXT("/Script/PAssaultShooter"), TEXT("ABaseWeapon"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABaseWeapon);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
